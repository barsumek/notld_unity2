﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 0.05f;
    public float rotateSpeed = 3f;
    public GameObject skillParticle;
    public GameObject specialCane;

    const string ANIMNAME = "AnimIndex";
    const int IDLE = 2;
    const int MOVE = 4;
    const int TURN = 6;
    const int ATTACK_SOUND = 12;
    const float TURN_TIME = 1f;

    bool isMoving;
    bool isChanneling;

    Animator anim;
    CharacterController con;
    QuerySoundController soundScript;
    Vector2 touchPosition;

	void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        con = GetComponent<CharacterController>();
        soundScript = GetComponent<QuerySoundController>();
        isMoving = false;
        isChanneling = false;
	}

    void Update()
    {
        // exit to menu
        if (Input.GetAxisRaw("Cancel") != 0)
        {
            GameManager.instance.QuitToMenu();
        }

        if (Input.GetAxisRaw("Jump") != 0 && !isChanneling)
        {
            isChanneling = true;
            anim.SetInteger(ANIMNAME, TURN);
            Invoke("Attack", TURN_TIME);
        }

        if (Input.GetAxisRaw("Fire1") != 0 && !isChanneling && GameManager.instance.HasSpecial())
        {
            Instantiate(specialCane, transform.position, Quaternion.identity);
            GameManager.instance.UseSpecial();
        }

        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");



        if ((moveX != 0.0f || moveZ != 0.0f) && !isChanneling)
        {
            if (!isMoving)
            {
                isMoving = true;
                anim.SetInteger(ANIMNAME, MOVE);
            }

            var moveDirection = new Vector3(moveX * moveSpeed * Time.deltaTime, 0f, moveZ * moveSpeed * Time.deltaTime);

            //rotate the player
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection), Time.deltaTime * 10);

            // move the character
            con.Move(moveDirection);
        }
        else if (isMoving && !isChanneling)
        {
            isMoving = false;
            anim.SetInteger(ANIMNAME, IDLE);
        }
        
	}

    void Attack()
    {
        skillParticle.SetActive(true);
        Invoke("DisableParticles", TURN_TIME);
        soundScript.PlaySoundByNumber(ATTACK_SOUND);
    }

    void DisableParticles()
    {
        isChanneling = false;
        isMoving = false;
        skillParticle.SetActive(false);
        anim.SetInteger(ANIMNAME, IDLE);
    }

}
