﻿using UnityEngine;
using System.Collections;

public class GameOverQuit : MonoBehaviour
{
	public void GoBackToMenu()
    {
        Application.LoadLevel(0);
    }
}
