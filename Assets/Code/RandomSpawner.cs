﻿using UnityEngine;
using System.Collections;

public class RandomSpawner : MonoBehaviour
{
    public Vector3 RandomCircle(Vector3 center, float radius)
    {
        // random angle
        float ang = Random.value * 360;

        Vector3 pos;

        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);

        return pos;
    }
}
