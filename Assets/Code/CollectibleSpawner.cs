﻿using UnityEngine;
using System.Collections;

public class CollectibleSpawner : RandomSpawner
{
    public float radius = 10f;
    public GameObject[] collectiblePrefabs;
    public GameObject healthCollectiblePrefab;

    void Start()
    {
        SpawnCollectible();
    }

    public void SpawnCollectible()
    {
        var position = RandomCircle(transform.position, radius);
        int randIndex = Random.Range(0,collectiblePrefabs.Length);
        Instantiate(collectiblePrefabs[randIndex], position, Quaternion.identity);
    }

    public void SpawnHealthCollectible()
    {
        var position = RandomCircle(transform.position, radius);

        Instantiate(healthCollectiblePrefab, position, Quaternion.LookRotation(transform.position));
    }
}
