﻿using UnityEngine;
using System.Collections;

public class GraveStonePick : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            var zombies = GameObject.FindGameObjectsWithTag("Zombie");
            foreach (var zombie in zombies)
            {
                zombie.GetComponent<ZombieController>().DeleteGrave(gameObject);
            }
            Destroy(gameObject);
        }
    }
}
