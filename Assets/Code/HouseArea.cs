﻿using UnityEngine;
using System.Collections;

public class HouseArea : MonoBehaviour
{
    void FixedUpdate()
    {
        var hitColliders = Physics.OverlapSphere(transform.position, 3.5f);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            var colObject = hitColliders[i].gameObject;
            if (colObject.tag.Equals("Enemy"))
            {
                colObject.GetComponent<EnemyAI>().Attack();
            }
            else if (colObject.tag.Equals("Zombie"))
            {
                colObject.GetComponent<ZombieController>().Attack();
            }
        }
    }
}
