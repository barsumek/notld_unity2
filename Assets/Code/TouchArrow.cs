﻿using UnityEngine;
using System.Collections;

public class TouchArrow : MonoBehaviour
{
    public float destroyTime = 1f;

    void Start()
    {
        Destroy(gameObject, destroyTime);
    }

}
