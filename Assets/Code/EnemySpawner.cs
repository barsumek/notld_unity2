﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : RandomSpawner
{
    public float radius = 14f;
    public float spawnDelay = 3.5f;
    public int bossSpawnCounter = 10;
    public float bossRadius = 12f;

    public float minSpawnDelay = 2.5f;
    public GameObject enemyPrefab;
    public GameObject bossPrefab;

    int bossCounter = 0;

    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }
    
    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnDelay);
            SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        bossCounter++;
        if (bossCounter >= bossSpawnCounter)
        {
            bossCounter = 0;
            SpawnBoss();
        }
        else
        {
            var randomPosition = RandomCircle(transform.position, radius);
            Instantiate(enemyPrefab, randomPosition, Quaternion.LookRotation(transform.position));
            if (spawnDelay > minSpawnDelay)
            {
                spawnDelay -= 0.01f;
            }
        }
    }

    void SpawnBoss()
    {
        var randomPosition = RandomCircle(transform.position, bossRadius);
        randomPosition.y += 3;
        Instantiate(bossPrefab, randomPosition, Quaternion.LookRotation(transform.position));
    }
}
