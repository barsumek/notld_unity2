﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    public void Play()
    {
        Application.LoadLevel(1);
    }

    public void Instruction()
    {
        Application.LoadLevel(3);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
