﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public CollectibleSpawner giftSpawner;
    public Text scoreText;
    public Text topScoreText;
    public Slider slider;
    public Image specialImage;

    int score = 0;
    int topScore;
    int counter = 0;
    int health = 100;

    bool special = true;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        special = true;
        topScore = PlayerPrefs.GetInt("Top Score", 0);
        topScoreText.text = topScore.ToString();
    }

    public void AddScore(int points)
    {
        score += points;
        scoreText.text = score.ToString();
        giftSpawner.SpawnCollectible();
        counter++;
        if (counter >= 10)
        {
            counter = 0;
            GetComponent<AudioSource>().Play();
            giftSpawner.SpawnHealthCollectible();
        }
        if (score > topScore)
        {
            topScore = score;
            topScoreText.text = topScore.ToString();
            PlayerPrefs.SetInt("Top Score", topScore);
        }
    }

    public void LoseHouseLife(int damage)
    {
        health -= damage;
        slider.value = health;
        if (health <= 0)
        {
            GameOver();
        }
    }

    public void AddHealth(int healthPoints)
    {
        var newHealth = health + healthPoints;
        health = newHealth > 100 ? 100 : newHealth;
        slider.value = health;
    }

    public void GetSpecial()
    {
        special = true;
        specialImage.enabled = true;
    }

    public void UseSpecial()
    {
        special = false;
        specialImage.enabled = false;
    }

    public bool HasSpecial()
    {
        return special;
    }

    public void QuitToMenu()
    {
        PlayerPrefs.Save();
        Application.LoadLevel(0);
    }

    void GameOver()
    {
        PlayerPrefs.Save();
        Application.LoadLevel(2);
    }

    void onApplicationQuit()
    {
        PlayerPrefs.Save();
    }
}
