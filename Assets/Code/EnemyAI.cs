﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    public int damage = 5;

    NavMeshAgent agent;
    Animation anim;
    AudioSource source;
    Transform target;

    const string DIE = "dead";
    const string ATTACK = "attack02";

    bool isAttacking;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animation>();
        source = GetComponent<AudioSource>();
        isAttacking = false;
    }

	void Start()
	{
        target = GameObject.FindGameObjectWithTag("House").transform;
        agent.SetDestination(target.position);
	}

    public void Attack()
    {
        if (isAttacking && anim.isPlaying)
            return;
        isAttacking = true;
        agent.enabled = false;
        var attack = anim.GetClip(ATTACK);
        anim.clip = attack;
        anim.Play();
        source.Play();
        GameManager.instance.LoseHouseLife(damage);
    }

    public void Die()
    {
        agent.enabled = false;
        var attack = anim.GetClip(DIE);
        anim.clip = attack;
        anim.Play();
        StartCoroutine(DestroyAfterDeath());
        GetComponent<Collider>().enabled = false;
    }

    IEnumerator DestroyAfterDeath()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
