﻿using UnityEngine;
using System.Collections;

public class CaneAttack : MonoBehaviour
{
    public GameObject shoot;
    public int caneTime = 10;

    void Start()
    {
        StartCoroutine(StartShooting());
    }

    IEnumerator StartShooting()
    {
        for (int i = 0; i < caneTime; i++)
        {
            TryShooting();
            yield return new WaitForSeconds(1f);
        }
        Destroy(gameObject);
    }

    void TryShooting()
    {
        var hitColliders = Physics.OverlapSphere(transform.position, 3f);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            var colObject = hitColliders[i].gameObject;
            if (colObject.tag.Equals("Enemy"))
            {
                var particleAttack = (GameObject) Instantiate(shoot, colObject.transform.position, Quaternion.identity);
                colObject.GetComponent<EnemyAI>().Die();
                Destroy(particleAttack, 0.4f);
            }
        }
    }


}
