﻿using UnityEngine;
using System.Collections;

public class HealthCollectiblePick : MonoBehaviour
{
    public int healthPoints = 50;

    const int PICK_SOUND = 38;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<QuerySoundController>().PlaySoundByNumber(PICK_SOUND);
            GameManager.instance.AddHealth(healthPoints);
            GameManager.instance.GetSpecial();
            Destroy(gameObject);
        }
    }
}
