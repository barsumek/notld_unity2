﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZombieController : MonoBehaviour
{
    public int damage = 10;

    NavMeshAgent nav;
    Transform target;
    Animator anim;

    bool isAlive;
    bool isAttacking;

    List<GameObject> gravestones = new List<GameObject>();
    bool checkGravestones;

	void Awake()
	{
        nav = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
	}

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("House").transform;
        checkGravestones = false;
        isAlive = true;
        isAttacking = false;
        nav.SetDestination(target.position);
    }

    void Update()
    {
        if (checkGravestones && gravestones.Count == 0)
        {
            Die();
        }
    }

    public void Attack()
    {
        if (isAttacking || !isAlive)
        {
            return;
        }
        isAttacking = true;
        nav.enabled = false;
        anim.SetTrigger("Attack");
        StartCoroutine(Damage());
    }

    public void Die()
    {
        if (!isAlive)
        {
            return;
        }
        isAlive = false;
        nav.enabled = false;
        anim.SetTrigger("Death");
        StartCoroutine(DestroyAfterDeath());
        GetComponent<Collider>().enabled = false;
    }

    IEnumerator DestroyAfterDeath()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }

    IEnumerator Damage()
    {
        while (isAlive)
        {
            GameManager.instance.LoseHouseLife(damage);
            yield return new WaitForSeconds(2f);
        }
    }

    public void AddGrave(GameObject gravestone)
    {
        gravestones.Add(gravestone);
        checkGravestones = true;
    }

    public void DeleteGrave(GameObject gravestone)
    {
        gravestones.Remove(gravestone);
    }
}
