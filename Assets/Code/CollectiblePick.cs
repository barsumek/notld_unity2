﻿using UnityEngine;
using System.Collections;

public class CollectiblePick : MonoBehaviour
{
    public int collectibleScore = 10;

    const int PICK_SOUND = 64;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<QuerySoundController>().PlaySoundByNumber(PICK_SOUND);
            GameManager.instance.AddScore(collectibleScore);
            Destroy(gameObject);
        }
    }
}
