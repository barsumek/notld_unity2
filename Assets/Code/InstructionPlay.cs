﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstructionPlay : MonoBehaviour
{
    public Sprite secondPage;
    bool exit = false;
	
	void Update()
	{
        if (Input.anyKey && !exit)
        {
            GetComponent<Image>().sprite = secondPage;
            Invoke("ChangeExit", 0.5f);
        }
        if (Input.anyKey & exit)
        {
            Application.LoadLevel(0);
        }
	}

    void ChangeExit()
    {
        exit = true;
    }
}
