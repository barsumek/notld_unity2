﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SummonBoss : RandomSpawner
{
    public GameObject bossPrefab;
    public float particleTime = 3f;
    public GameObject gravestonePrefab;
    public float gravestonesRadius = 2.5f;

	void Start()
	{
        StartCoroutine(Summon());
	}
	
	IEnumerator Summon()
    {
        yield return new WaitForSeconds(particleTime);
        var bossSpawn = transform.position;
        bossSpawn.y -= 3;
        var boss = (GameObject) Instantiate(bossPrefab, bossSpawn, Quaternion.identity);
        yield return new WaitForSeconds(particleTime / 2);

        for (int i = 0; i < 3; i++)
        {
            var randomPosition = RandomCircle(bossSpawn, gravestonesRadius);

            var grave = (GameObject)Instantiate(gravestonePrefab, randomPosition, Quaternion.Euler(270, Random.Range(0, 360), 0));
            boss.GetComponent<ZombieController>().AddGrave(grave);
        }
        Destroy(gameObject);
    }
}
