﻿using UnityEngine;
using System.Collections;

public class SkillAttack : MonoBehaviour
{
	void FixedUpdate()
	{
		var hitColliders = Physics.OverlapSphere(transform.position, 4f);
		for (int i = 0; i < hitColliders.Length; i++)
		{
			var colObject = hitColliders[i].gameObject;
			if (colObject.tag.Equals("Enemy"))
			{
				colObject.GetComponent<EnemyAI>().Die();
			}
		}
	}
}
